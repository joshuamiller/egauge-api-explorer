﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace eGaugeDeviceAPIExplorer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        const string StoredXMLPath = "cgi-bin/egauge-show";
        const string InstantaneousXMLPath = "cgi-bin/egauge";
        private CancellationTokenSource _cts = new CancellationTokenSource();

        public MainWindow()
        {
            InitializeComponent();
        }

        private string getParams()
        {
            string paramString = "";
            if (textBoxRowLimit.IsEnabled && textBoxRowLimit.Text.Length > 0)
                paramString = "n=" + textBoxRowLimit.Text;
            if (checkBoxVirtual.IsEnabled && (checkBoxVirtual.IsChecked ?? false))
                paramString += "&a";
            if (checkBoxCSV.IsEnabled && (checkBoxCSV.IsChecked ?? false))
                paramString += "&c";
            if (checkBoxDelta.IsEnabled && (checkBoxDelta.IsChecked ?? false))
                paramString += "&C";
            paramString += textBoxParameterString.Text;
            return paramString;
        }

        private string getAPIPath()
        {
            if (checkBoxInstantaneous.IsChecked ?? false)
                return InstantaneousXMLPath;

            return StoredXMLPath;
        }

        private string getURL()
        {
            string proxy = textBoxDomain.Text;
            if (proxy.Length > 0)
                proxy = "." + proxy;
            string device = textBoxDeviceName.Text;

            return String.Format("{0}://{1}{2}/{3}?{4}", "http", device, proxy, getAPIPath(), getParams());
        }

        private void cancelRequest()
        {
            _cts.Cancel();
            _cts = new CancellationTokenSource();
        }

        private CredentialCache getCredentials(string url)
        {
            var credCache = new CredentialCache();

            credCache.Add(new Uri(url), "Digest", new NetworkCredential(textBoxAuthUser.Text, textBoxAuthPW.Text));
            return credCache;
        }

        async Task Request(string url)
        {
            using (var client = new HttpClient(new HttpClientHandler { Credentials = getCredentials(url) }))
            {
                textBlockRequestStatus.Text = "Waiting for response...";
                HttpResponseMessage response = await client.GetAsync(url, _cts.Token);
                textBlockRequestStatus.Text = "";

                textBlockResponse.Text = response.StatusCode.ToString();
                if (response.IsSuccessStatusCode)
                {
                    textBlockResponse.Foreground = Brushes.Green;
                    textBoxResponse.Text = await response.Content.ReadAsStringAsync();
                }
                else
                {
                    textBlockResponse.Foreground = Brushes.Red;
                }
                buttonQuery.IsEnabled = true;
            }
        }

        private void buttonsStartQuery()
        {
            buttonQuery.IsEnabled = false;
            buttonCancel.IsEnabled = true;
        }

        private void buttonsCancelQuery()
        {
            buttonQuery.IsEnabled = true;
            buttonCancel.IsEnabled = false;
        }

        private void resetText()
        {
            textBlockRequestStatus.Text = "";
            textBoxResponse.Text = "";
            textBoxGeneratedURL.Text = "";
        }

        private void buttonQuery_Click(object sender, RoutedEventArgs e)
        {
            resetText();

            if (textBoxDeviceName.Text.Length == 0)
            {
                textBlockRequestStatus.Text = "Device Name required!";
                return;
            }

            string url = getURL();
            buttonsStartQuery();
            textBoxGeneratedURL.Text = url;

            Request(url);
        }

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            resetText();
            cancelRequest();
            buttonsCancelQuery();
        }

        private void options_Toggle(bool state)
        {
            checkBoxVirtual.IsEnabled = state;
            checkBoxCSV.IsEnabled = state;
            checkBoxDelta.IsEnabled = state;
            textBoxRowLimit.IsEnabled = state;
        }

        private void checkBoxInstantaneous_Checked(object sender, RoutedEventArgs e)
        {
            options_Toggle(!checkBoxInstantaneous.IsChecked ?? false);
        }

        private void checkBoxInstantaneous_Unchecked(object sender, RoutedEventArgs e)
        {
            options_Toggle(!checkBoxInstantaneous.IsChecked ?? false);
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }
}
